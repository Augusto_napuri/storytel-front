var view = {
    showMessages: function (messages) {
        $.get("message.html", function (page) {
            var tbody = [];
            var closeTr = "</tr>";
            messages.forEach(function (e, i) {
                tbody.push("<tr class='border_bottom' style='border-bottom:2px solid #005000;' id=message" + e.id + ">");
                tbody.push(view.createRow(e).join(""));
                tbody.push(closeTr);
            });
            tbody = tbody.join("");
            $("#page").html(page);
            $("tbody").html(tbody);
        });
    },

    addMessage: function (message) {
        var closeTr = "</tr>";

        var tbody = view.createRow(message);
        tbody.unshift("<tr id=message" + message.id + ">");
        tbody.push(closeTr);
        tbody = tbody.join("");
        $("tbody").append(tbody);
    },

    editMessage: function (message) {
        var tbody = view.createRow(message);
        tbody = tbody.join("");
        $("#message" + message.id).html(tbody);
    },

    showUsername: function (username) {
        $("#greeting h1").html("Greetings " + username);
    },

    removeMessage: function (id) {
        $("#message" + id).remove();
    },

    createRow: function (message) {
        var tbody = [];
        var openTd = "<td style='text-align: center'><span>";
        var closeTd = "</span></td>";
        tbody.push(openTd);
        tbody.push(message.username);
        tbody.push(closeTd);

        tbody.push(openTd);
        tbody.push(message.dateCreated.replace("T", " "));
        tbody.push(closeTd);

        tbody.push(openTd);
        if (message.edited) {
            tbody.push("&#10004");
        } else {
            tbody.push("&#10006");
        }
        tbody.push(closeTd);


        tbody.push(openTd);
        tbody.push(message.text);
        tbody.push(closeTd);

        if (message.owner) {
            tbody.push(openTd);
            tbody.push("<button onclick=service.delete(" + message.id + ")><i class='material-icons'>cancel</i></button>");
            tbody.push(closeTd);
            tbody.push(openTd);
            tbody.push("<input id='editMessage" + message.id + "'/><button onclick=service.edit(" + message.id + ")><i class='material-icons'>edit</i></button>");
            tbody.push(closeTd);
        }else {
            tbody.push(openTd);
            tbody.push(closeTd);
            tbody.push(openTd);
            tbody.push(closeTd);
        }
        return tbody;
    }
};



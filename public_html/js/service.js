var service = {
    url: 'http://localhost:8081/api',
    
    get: function () {
        model.username = $("#username").val();
        $.ajax({
            url: service.url + '?username=' + $("#username").val(),
            type: 'GET',
            contentType: 'application/json',
            headers: {"Access-Control-Allow-Origin": "*"},
            error: function (jqXHR, textStatus, errorThrown) {
                alert("Error");
            },
            success: function (data, textStatus, jqXHR) {
                view.showUsername($("#username").val());
                view.showMessages(jqXHR.responseJSON);
            }
        });
    },

    add: function () {
        model.text = $("#addMessage").val();
        $.ajax({
            url: service.url ,
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(model),
            headers: {"Access-Control-Allow-Origin": "*"},
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jqXHR.responseJSON.message);
            },
            success: function (data, textStatus, jqXHR) {
                view.addMessage(jqXHR.responseJSON);
            }
        });
    },

    delete: function (id) {
        model.id = id;
        $.ajax({
            url: service.url,
            type: 'DELETE',
            contentType: 'application/json',
            data: JSON.stringify(model),
            headers: {"Access-Control-Allow-Origin": "*"},
            error: function (jqXHR, textStatus, errorThrown) {
                model.id = null;
                alert(jqXHR.responseJSON.message);
            },
            success: function (data, textStatus, jqXHR) {
                model.id = null;
                view.removeMessage(id);
            }
        });
    },
    
    edit: function(id) {
        model.id = id;
        model.text = $("#editMessage" + id).val();
        $.ajax({
            url: service.url,
            type: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(model),
            headers: {"Access-Control-Allow-Origin": "*"},
            error: function (jqXHR, textStatus, errorThrown) {
                model.id = null;
                alert(jqXHR.responseJSON.message);
            },
            success: function (data, textStatus, jqXHR) {
                model.id = null;
                view.editMessage(jqXHR.responseJSON);
            }
        });
    }
};
